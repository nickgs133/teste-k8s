package com.mastertech.testek8s.version;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/version")
public class VersionController {

    @Value("${app.version}")
    private int version;

    @GetMapping
    public Map<String, Integer> getVersion() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("version", version);
        return map;
    }
}
