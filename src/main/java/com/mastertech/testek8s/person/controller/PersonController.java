package com.mastertech.testek8s.person.controller;

import com.mastertech.testek8s.person.model.Person;
import com.mastertech.testek8s.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping
    public Iterable<Person> listPerson() {
        return personService.listPerson();
    }

    @PostMapping
    public Person createPerson(@RequestBody Person person) {
        return personService.create(person);
    }
}
