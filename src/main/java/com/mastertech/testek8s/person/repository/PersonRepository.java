package com.mastertech.testek8s.person.repository;

import com.mastertech.testek8s.person.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
