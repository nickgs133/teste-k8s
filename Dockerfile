FROM openjdk:8-jdk-alpine

WORKDIR /app
COPY ./target/teste-k8s-0.0.1-SNAPSHOT.jar .

EXPOSE 8080/tcp

ENTRYPOINT ["java","-jar","teste-k8s-0.0.1-SNAPSHOT.jar"]
